import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import javax.imageio.ImageIO;

public class Steganography {
	
	/*
	 * ==================================
	 * EMBEDDING METHODS
	 * ==================================
	 */
	
	/**
	 * This method is used to embed the desired message and the message length. We embed the message length 
	 * first so that when extraction is attempted, we know how many bits to read.
	 * @param img
	 * @param msg
	 */
	public static void embedEntireMsg(BufferedImage img, String msg) {
		int msgLength = msg.length();
		
		int imgWidth = img.getWidth();
		int imgHeight = img.getHeight();
		int imgSize = imgWidth * imgHeight;
		
		// Check our message + message length can fit inside the BMP image
		if ((msgLength * 8) + 32 <= imgSize) {
			// Embed message length int
			embedMsgLength(img, msgLength);
			
			byte arr[] = msg.getBytes();
			// Embed each character of the message
			for (int i = 0; i < arr.length; i++) {
				embedCharacter(img, arr[i], i*8+32);
			}
		}
	}
	
	/**
	 * This method embeds the length of the message into the image. This is embedded value is used in the extraction process to
	 * identify how many sequential bits to read from the bitmap image.
	 * @param img - the vessel image
	 * @param msgLength - the length of the message
	 */
	public static void embedMsgLength(BufferedImage img, int msgLength) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();
		
		int startX = 0;
		int startY = 0;
		
		// The current bit number of the binary representation of the message length
		int bitLocation = 0;
		
		int x = startX;
		int y = startY;
		
		// Embed the payload length int across the first 32 least significant bits (int in Java is 32-bit)
		while (x < maxX && y < maxY && bitLocation < 32) {
			int rgb = img.getRGB(x, y);
			int bitToEmbed = getBitValue(msgLength, bitLocation);
			rgb = setBitValue(rgb, 0, bitToEmbed);
			
			img.setRGB(x, y, rgb);
			
			// Move to embed the next bit of the message length int
			bitLocation++;
			
			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}
	}
	
	/**
	 * Since we are embedding a string, we must embed each character (byte) of the string message. This method embeds one character into the image.
	 * 
	 * @param img - the vessel image
	 * @param b - the character (byte) to embed
	 * @param start - the bit number that we start the embedding process at. We don't start from the beginning as we must first embed the message length.
	 */
	public static void embedCharacter(BufferedImage img, byte b, int start) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();
		
		int startX = start/maxY;
		int startY = start - startX*maxY;
		
		// The current bit number of the binary representation of the character (byte)
		int bitLocation = 0;
		
		int x = startX;
		int y = startY;
		
		// Embed the next payload character across the next 8 least significant bits
		while (x < maxX && y < maxY && bitLocation < 8) {
			int rgb = img.getRGB(x, y);
			int bitToEmbed = getBitValue(b, bitLocation);
			
			rgb = setBitValue(rgb, 0, bitToEmbed);
			
			img.setRGB(x, y, rgb);
			
			// Move to embed the next bit of the character
			bitLocation++;
			
			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}
	}
	
	/*
	 * ==================================
	 * EXTRACTION METHODS
	 * ==================================
	 */
	
	public static String extractEntireMsg(BufferedImage img) {
		int len = extractMsgLength(img, 0);
        byte b[] = new byte[len];
        
        for (int i = 0; i < len; i++) {
        	b[i] = extractCharacter(img, i*8+32);
        }
        
        return new String(b);
	}
	
	/**
	 * This method extracts the length of the message from the file. This is used to know how
	 * long to read the file for the extraction process.
	 * 
	 * @param img - vessel image
	 * @param start - the bit number in the BMP that we start extracting from
	 * @return the length of the embedded message
	 */
	public static int extractMsgLength(BufferedImage img, int start) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();
		
		int startX = start/maxY;
		int startY = start - startX*maxY;
		
		int length = 0;
		
		// The current bit number of the binary representation of the message length int
		int bitLocation = 0;
		
		int x = startX;
		int y = startY;
		
		// Extract the payload length int from the first 32 least significant bits (int in Java is 32-bit)
		while (x < maxX && y < maxY && bitLocation < 32) {
			int rgb = img.getRGB(x, y);
			int bit = getBitValue(rgb, 0);
			length = setBitValue(length, bitLocation, bit);
			
			// Move to extract the next bit of the int
			bitLocation++;
			
			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}
		
		return length;
			
	}
	
	/**
	 * This method extracts one character from the provided image.
	 * 
	 * @param img - vessel image
	 * @param start - the bit number in the BMP that we start extracting from
	 * @return
	 */
	public static byte extractCharacter(BufferedImage img, int start) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();
		
		int startX = start/maxY;
		int startY = start - startX*maxY;
		
		// The current bit number of the binary representation of the character
		int bitLocation = 0;
		
		// This is where we extract the bits of the character to
		byte b = 0;
		
		// Extract the next payload character from the next 8 least significant bits
		while (x < maxX && y < maxY && bitLocation < 8) {
			int rgb = img.getRGB(x, y);
			
			// Get Least Significant Bit of RGB Value
			int bit = getBitValue(rgb, 0);
			
			// Add the bit to the current byte
			b = (byte) setBitValue(b, bitLocation, bit);
			
			// Move to extract the next bit of the character
			bitLocation++;
			
			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}
		
		return b;
		
	}
	
	/*
	 * ==================================
	 * HELPER METHODS
	 * ==================================
	 */
	
	/**
	 * This method takes a decimal number and a bit location for the integer. The method
	 * then returns the value of the bit (0 or 1) for the location, for the decimal number provided.
	 * 
	 * EXAMPLE
	 * n = 6
	 * intLocation = 3
	 * 
	 * Method will return 0
	 * 
	 * Bit Location:  3 2 1 0
	 * Binary:        0 1 1 0
	 * 
	 */
	public static int getBitValue(int n, int location) {
		int value = (int) (n & Math.round(Math.pow(2, location)));
		return value == 0 ? 0 : 1;
	}
	
	/**
	 * This method embeds information using the least significant bit method. Due to this 
	 * minimal change will be noticed within the updated bitmap image. Since we are using bitwise
	 * operations, this method can be ran again to retrieve the embedded information.
	 * 
	 * @param n - The RGB value that the information will be embedded in
	 * @param location - the bit location in n that bitToEmbed is embedded
	 * @param bit - the bit to embed into n
	 * @return the updated RGB value
	 */
	public static int setLSB(int n, int location, int bit) {
		
		// Get the least significant bit of the RGB value
		int lsb = getBitValue(n, location);
		int value = (int) Math.pow(2, location);
		
		// If LSB and provided bit are already the same, the information is already accurate and we don't need to do anything.
		if (lsb == bit) {
			return n;
		}
		
		// If LSB and provided bit are different, then we set the LSB to the provided bit using AND bitwise operation
		if (lsb == 0 && bit == 1) {
			n |= value;
		}
		
		// If LSB and provided bit are different, then we set the LSB to the provided bit using XOR bitwise operation
		else if (lsb == 1 && bit == 0) {
			n ^= value;
		}

		// Return the byte with the updated LSB
		return n;
	}
	
	/*
	 * ==================================
	 * setPath
	 * ==================================
	 * This method makes sure sets the path for the images that will be used in the code. 
	 * This is necessary so that the code can be ran by anyone who downloads the code files without them having to change the path. 
	*/
    public static String setPath(){
        String basePath = new File("").getAbsolutePath();
        return basePath + File.separator + "images" + File.separator;
    }

	public static String setPathTests(){
		String basePath = new File("").getAbsolutePath();
		return basePath + File.separator + "test_imgs" + File.separator;
	}

	/*
	 * ==================================
	 * determineBehaviourBasedOnCommandLineArguments
	 * ==================================
	 * This method considers the command line arguments given by the user and runs the appropriate methods based on this.
	 */
	public static void determineBehaviourBasedOnCommandLineArguments(String[] args) throws IOException{
		if (args.length > 0) {
			if(args[0].equals("embed")){
				BufferedImage img = ImageIO.read(new File(setPath()+args[1]));
				String payload = Files.readString(Path.of(args[2]));
				embedEntireMsg(img, payload);
				System.out.println("Successfully embedded payload into: " + setPath()+args[1]);
				ImageIO.write(img, "bmp", new File(setPath()+args[1]));
			}
			else if(args[0].equals("extract")){
				BufferedImage stegImg = ImageIO.read(new File(setPath()+args[1])); // Put path to embedded image here
				String payload = extractEntireMsg(stegImg);
				Files.writeString(Path.of("extracted.txt"), payload, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
				System.out.println("Payload extracted to: extracted.txt");
				System.out.println(payload);
			}
			else if(args[0].equals("help")){
				System.out.println("Input command should have one of the following forms:\n\tjava Steganograpy embed target_filename.bmp message_filename.txt\n\tjava Steganography extract target_filename.bmp\n\tjava Steganography help");
			}
			else{
				System.out.println("Invalid command: command should have one of the following forms:\njava Steganograpy embed target_filename.bmp message_filename.txt\njava Steganography extract target_filename.bmp");
			}
		}
		
		else {
			System.out.println("Invalid command: command should have one of the following forms:\njava Steganograpy embed target_filename.bmp message_filename.txt\njava Steganography extract target_filename.bmp");
		}
	}
	
	public static void main(String[] args) throws IOException {
		Steganography.determineBehaviourBasedOnCommandLineArguments(args);
	}
}
