
- [ ] Report
	- [ ] detailed test cases (3 expected and 2 extreme)
	- [ ] setup and operation instructions (java version, build numbers etc.)
	- [ ] bibliography (add whatever research sources we used)
	- [ ] appendix with commented and structured code that demonstrates intellectual input, recognition of the problem and coverage of key steps.
- [ ] Workload Report
- [ ] Report Formatting Checklist
	- [x]  report is a PDF file
	- [x]  team name and members on cover-page
	- [x]  filename is in format of matrice numbers of team members (i.e. 2383199_2375346_2523558.pdf)
	- [x]  12-point font, 1-inch margins
- [ ] Presentation
	- [x] cover slide introducing team, names, student numbers and length (time needed to give presentation?)
	- [x] recognition of the problem with an overview of anti-forensics and associated challenges with specific focus on stenography. (Heather)
	- [ ] coverage of key steps in the process through explanation and demonstration of the lightweight Java application (Robbie)
	- [ ] describe bitmap images and argue the motivation for engineer to opt for the format as a vessel (Reuben)
	- [ ] articulate the importance of the header within the file and how to embed the size of the payload within the vessel as well as how this supports recovery of the payload and varying payload sizes (Robbie - within main section)
	- [ ] describe how the payload will be converted into bits in Java and how these bits are embedded within the vessel (Robbie - within main section)
	- [ ] demonstrate test cases, specifically the entire process of embedding a payload within a vessel and then recovering it from it as well as recovering a payload from another bitmap vessel (Robbie - within main section)
	- [ ] argue whether the former software engineer would have indeed exfiltrated data in such way from within the corporation (Reuben)
	- [ ] argue whether the case sought by HRUK is even viable, i.e. specifically the software engineer using company workstations to smuggle code for the public interest and consider impact on source code intellectual property rights and freedom of information (hint: consider the external context and strengthen argument with relevant evidence). (Heather)

	- **some side-notes**: we have 10 min max (plus or minus 1 minute)
		- if we all take a few sections to speak on we have roughly 3 min 30 to cover (minimum of 3 min each if we would rather speak less)
		- average speaking rate is 150 wpm, so that means we should aim to have 525 words per person
			- roughly 1500 words total in the draft for the final presentation
	- teams must ensure the video is accessible and can be **downloaded** (what does this mean for us, is there no option to upload .mp4 files, and if so where do we upload the video)


### Submission
- [ ] Finished Report (including source code) + Workload Report
- [ ] Presentation Video should be 10 minutes in length (plus or minus 1 minute)
- [ ] Personal assessment of contribution (submitted separately)
